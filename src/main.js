import { SVG } from '@svgdotjs/svg.js'
import { defineHex, Grid, rectangle } from 'honeycomb-grid'


const tools = ['on-tool', 'off-tool', 'connect-tool']

let state = {
    starts: [],

    hexes: {

    }
}
let clicked = false;
let selected_tool;
let selected_hex = undefined;

const exportJSON = (current_state) => {
    result = {
        ...current_state,
        hexes: []
    }

    for (var hex_index in current_state['hexes']) {
        if (current_state['hexes'][hex_index].state == true) {
            result['hexes'].push({
                index: hex_index,
                previous: current_state['hexes'][hex_index].previous,
                next: current_state['hexes'][hex_index].next
            })
        }
    }

    document.getElementById("json").value = JSON.stringify(result)
}

const selectTool = (tool_to_use) => {
    tools.map(tool_x => {
        document.getElementById(tool_x).classList.remove('bg-transparent')
        document.getElementById(tool_x).classList.remove('bg-blue-400')
    })
    document.getElementById(tool_to_use).classList.add('bg-blue-400')
    selected_tool = tool_to_use
}

document.getElementById('on-tool').addEventListener('click', () => {
    selectTool('on-tool')
    selected_hex = undefined
})

document.getElementById('off-tool').addEventListener('click', () => {
    selectTool('off-tool')
    selected_hex = undefined
})

document.getElementById('connect-tool').addEventListener('click', () => {
    selectTool('connect-tool')
})

document.getElementById('hexgrid').addEventListener('mousedown', ({ offsetX, offsetY }) => {
    clicked = true
})

const connect = (from, to) => {
    state['hexes'][[from['q'], from['r']]].next.push([to['q'], to['r']]);
    state['hexes'][[to['q'], to['r']]].previous.push([from['q'], from['r']])

    const center_from_x = from.corners.map((a) => a['x']).reduce((a, b) => a + b, 0) / 6
    const center_from_y = from.corners.map((a) => a['y']).reduce((a, b) => a + b, 0) / 6
    const center_to_x = to.corners.map((a) => a['x']).reduce((a, b) => a + b, 0) / 6
    const center_to_y = to.corners.map((a) => a['y']).reduce((a, b) => a + b, 0) / 6

    console.log(center_from_x, center_from_y, center_to_x, center_to_y)

    draw.line(center_from_x, center_from_y, center_to_x, center_to_y).stroke({width: 2, color: "#000"})
}   


document.getElementById('hexgrid').addEventListener('mouseup', ({ offsetX, offsetY }) => {
    clicked = false

    console.log(selected_tool)
    if (selected_tool == 'connect-tool') {
        const hex = grid.pointToHex(
            { x: offsetX, y: offsetY },
            { allowOutside: false }
        )

        if (hex === undefined) return;

        if (selected_hex !== undefined) {
            
            connect(selected_hex, hex);

            selected_hex = undefined;
        } else {
            selected_hex = hex;
        }
    }
    exportJSON(state)
})



document.getElementById('hexgrid').addEventListener(
    "mousemove",
    ({ offsetX, offsetY }) => {

        if (clicked) {

            const hex = grid.pointToHex(
                { x: offsetX, y: offsetY },
                { allowOutside: false }
            )

            if (selected_tool == 'on-tool') {
                state['hexes'][[hex['q'], hex['r']]].state = true
                state['hexes'][[hex['q'], hex['r']]].polygon.fill('#0A0')
            }

            if (selected_tool == 'off-tool') {
                state['hexes'][[hex['q'], hex['r']]].state = false
                state['hexes'][[hex['q'], hex['r']]].polygon.fill('none')
            }

            exportJSON(state)
            
            
        }

        // prevent default to allow drop
        event.preventDefault();
    },
    false
);

// you may want the origin to be the top left corner of a hex's bounding box
// instead of its center (which is the default)
const Hex = defineHex({ dimensions: 15, origin: 'topLeft' })
const grid = new Grid(Hex, rectangle({ width: 30, height: 30 }))

const draw = SVG().addTo('#hexgrid').size('100%', '100%')


const initializeGrid = (grid) => {

    for (const hex of grid) {

        const polygon = draw
            // create a polygon from a hex's corner points
            .polygon(hex.corners.map(({ x, y }) => `${x},${y}`))
            .fill('none')
            .stroke({ width: 2, color: '#999' })




        state['hexes'][[hex['q'], hex['r']]] = {
            state: false,
            next: [],
            previous: [],
            polygon
        }

        // const text = draw.text('0').font({size: 12, anchor: 'middle', leading: '1.5em'}).fill('#000');
        // text.move(center.x, center.y).dx(-text.length() / 2).dy(-text.bbox().height / 2);
    }

}

selectTool('on-tool')
initializeGrid(grid)

